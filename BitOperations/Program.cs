﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace BitOperations
{
	/* Пример динамически расширяемого битового контейнера.
	 * Данная реализация не потокобезопасна! */
	class BitContainer {
		protected List<byte> bytes;
		private int countBits = 0;
		public int Length {
			get { return countBits; }
		}
		public BitContainer (int initialCapacity) {
			bytes = new List<byte>(initialCapacity);
		}
		public int getBit(int nbit) {
			int index = nbit / 8;
			if (index >= bytes.Count) //countBits
				throw new IndexOutOfRangeException("getBit: index out of range");
			return (bytes[index] >> nbit % 8) & 1;
		}
		public void setBit(int nbit, int value) {
			int index = nbit / 8, 
			offset = nbit % 8;
			bytes[index] = (byte) (bytes[index] & ~(1 << offset) | (value << offset));
		}
		public BitContainer pushBit(int value) {
			// Not a thread safe!
			if (countBits + 1 > bytes.Count * 8)
				bytes.Add(0); // need to add new byte to list
			setBit(countBits++, value);
			return this;
		}
		public BitContainer pushBit(bool value) {
			pushBit(value ? 1 : 0);
			return this;
		}
		public void Clear() {
			this.countBits = 0;
		}
		public override string ToString() {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < countBits; i++)
				sb.Append(getBit(i)!=0?'1':'0');
			return sb.ToString();
		}
	}

	class BufferedBitWriter : BitContainer {
		private FileStream f;
		public const int DefaultBufSize = 256;
		private int bufSize;

		public FileStream FStream {
			get { return f; }
		}
		public BufferedBitWriter(FileStream f, int bufSize) 
			: base(bufSize * 8) {
			this.f = f;
			this.bufSize = bufSize;
		}
		public BufferedBitWriter(FileStream f)
			: this(f, BufferedBitWriter.DefaultBufSize) {
		}
		public void Write(BitContainer b) {
			//Будем переносить такое кол-во бит, сколько возможно до заполнения буфера
			int nbit = 0;
			do {
				int bitsToWrite = this.bufSize * 8 - this.Length; //остаточная вместимость
				//Console.WriteLine("canWrite {0}", bitsToWrite);
				for (; nbit < Math.Min(bitsToWrite, b.Length); nbit++)
					this.pushBit(b.getBit(nbit));
				if (this.Length >= bufSize * 8) { // заполнили буфер целиком?
					f.Write(this.bytes.ToArray(), 0, bufSize);
					this.Clear();
				}
			} while (nbit < b.Length); //пока не перенесём все
		}

		public void Flush() {
			// Flush the rest of bits
			// TODO: Написать самостоятельно в качестве упражнения
		}
	}

	class MainClass {
		public static void Main(string[] args) {
			//Создадим битовый контейнер начальной вместимости 10
			/*BitContainer btc = new BitContainer(10);
			Console.WriteLine(btc.ToString());
			for (int i = 0; i < 1000; i++)
				btc.pushBit(i%2 != 0);*/
			//Console.WriteLine(btc.ToString());	

			//отличие в поведении встроенного BitArray в том, что он должен быть фиксированным
			/*BitArray bits = new BitArray(100);
			for (int i = 0; i < 100; i++)
				bits.Set(i, i % 2 != 0);
			for (int i = 0; i< bits.Count; i++)
				Console.Write(bits.Get(i));*/
			/*SomeValType s = new SomeValType((Int16) 1000);
			Console.WriteLine("{0} {1}",s.m_b, s.m_x);*/


			FileStream fs = new FileStream("out.bin", 
				FileMode.Create);
			BufferedBitWriter writer = new BufferedBitWriter(fs, 16);
			BitContainer b = new BitContainer(8);
			b.pushBit(1).pushBit(0).pushBit(0); //chaining methods
			for (int i=0; i<10000000; i++)
				writer.Write(b);
			writer.Flush();
			writer.FStream.Close();
		}
	}
}
